from flask import Flask
from flask import render_template
import random
import requests
app = Flask(__name__)



@app.route('/')
def base():
    response=requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    dinos=response.json()
    return render_template('dino.html',dinos=dinos)

@app.route('/dinosaurs/')
@app.route('/dinosaurs/<slug>')
def dino(slug):
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    dinos = response.json()
    for dino in dinos:
        if (dino.get("slug")) == slug:
            r=dino.get("uri")
            responsed=requests.get(str(r))
            dino_slug=responsed.json()
            dino_random = random.sample(dinos,3)
            return render_template('detaildino.html', dino_slug=dino_slug,dino_random=dino_random)





