import pytest
import requests

def dinolist():
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    dinolist = response.json()
    return dinolist

def dinolist_count():
    dinolist_count= dinolist()
    assert 8 == len(dinolist)
