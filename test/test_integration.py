import os
import pytest
import requests
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

def test_dino_index(client):
    rv = client.get('/')
    assert rv.status_code == 200

def test_detaildino(client):
    rv = client.get('/dinosaurs/velociraptor')
    assert rv.status_code == 200
    assert b"velociraptor" in rv.data
    assert b"https://allosaurus.delahayeyourself.info/static/img/dinosaurs/velociraptor.jpg"in rv.data

def test_detaildino2(client):
    rv = client.get('/dinosaurs/triceratops')
    assert rv.status_code == 200
    assert b'triceratops' in rv.data
    assert b'https://allosaurus.delahayeyourself.info/static/img/dinosaurs/triceratops.jpg' in rv.data